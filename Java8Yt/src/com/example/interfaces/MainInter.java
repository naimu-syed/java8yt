package com.example.interfaces;

@FunctionalInterface
public interface MainInter{
	
	public abstract void sayHello();
	
	
//	we can use this in classes in 3 methods
	
	
//	method 1 : Create Separate Class and Implements This interface 
	/*
	 * First we need to create a separate Class apart from main class 
	 * and implement that another class with interface and use it in main Class 
	 * that you can see in SecondClass
	 */
	
	
}