package com.example.classes;

public class ExampleLambda {

public static void main(String[] args) {
	
//	for example we have a function 
	/*
	 * public void m1(){
	 * System.out.println("Test")};
	 */
	
	
	/*
	 * we can write the above function using lambda as 
	 * 
	 * ()->{System.out.println("Test")};  like this 
	 * 
	 */
	
	
//	we can remove {} braces if code is single line only 
//	for multiple lines we need braces 
//	but for single line braces are optional
	
	
//	so let us view the Rules of Lambda 
	/*
	 * Reduces lines of code 
	 * sequential and parallel exception support 
	 * by passing behavior as argument in methods
	 * to call method api's very effectively
	 * to write more readable maintainable and concise code 
	 * concise means remove unnecessary code or repeated code removal 
	 */
	
//	Important Rules to it Must Remember 
	
/*
 * If the body of Lambda Expression Contain only one Statement like above 
 * example of sum then curly braces are optional
 * 
 * Java Compiler Also insert the Type of variable Passed in arguments ,
 *  hence type is optional
 */
	
	
//	Another Example 2
	
//	public int sum(int a, int b) {
//		return (a+b);
//	}
	
	/*
	 * we can write this above code as follows
	 * 
	 * (a,b)->{(a+b)};
	 * 
	 */
//	Another Example 3
	
//	public int getLength(String str) {
//		return str.length();
//	}
	
	/*
	 * we can write this above code as follows
	 * 
	 * (str)-> str.length("Naimuddin");
	 * 
	 */
	
	
// Now we can see these in practically
	
}
	
	
}
