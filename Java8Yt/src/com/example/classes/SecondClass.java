package com.example.classes;

import com.example.interfaces.MainInter;

public class SecondClass implements MainInter {

//Method 1: to use this in main Class we need to override the interface
	
	@Override
public void sayHello() {

System.out.println("Hello !  This is From My MyInter Interface Implementing In Main Class Using Override in Second Class ");		
		
}

}
