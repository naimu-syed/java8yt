package com.example.classes;

import com.example.interfaces.LambdaMain;

public class LambdaClass {

	public static void main(String[] args) {
		
//		LambdaMain main = (a,b) -> (a+b);
//		
//
//		System.out.println(main.sum(5, 5));
//		By using Lambda we can Use this as Simple 
//		
//		System.out.println(main.sum(10, 5));
	
	
		LambdaMain main = (str)->str.length();
		System.out.println(main.getlength("Naimuddin"));
		 
//		 easily
//		we can see this as lambdas 
//			we can also create threads using Lambdas
		
		
//		Thread 1
		
		Runnable runnable = () -> {
//			this is the body of thread
			for(int i = 1; i <= 10; i++) {
				System.out.println("Value of I is "+i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
		};
		
		Thread t = new Thread(runnable);
		t.setName("John");
		t.start();
		
		
//		Thread 2:
		
		Runnable run = () -> {
			for (int i=1;i<11;i++) {
				System.out.println(i*2);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
	
	Thread thread2 = new Thread(run);
	thread2.start();
	
	}
	

}
