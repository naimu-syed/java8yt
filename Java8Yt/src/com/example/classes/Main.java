package com.example.classes;

import com.example.interfaces.MainInter;

public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("This is From Main Class Without Interface Implementing");
		
	
//		the Above is Normal main Class which is Nothing but main Method and Print Statement
		
//		Now we use Interface Implementing using method 1:
		
/*
 * First we need to Create Object For that
 * 
 */

		SecondClass class1 = new SecondClass();
		class1.sayHello();
		
//		Disadvantage is if we need to create 100 implementation we need to create 100 objects
		
//		Method 2:  By Using Anonymous Class we can Implements Interface without any Extra Class like (Second Class) 
//		we Can Directly Implement it
		
		MainInter inter=new MainInter() {
			
			@Override
			public void sayHello() {
				// TODO Auto-generated method stub
				
				System.out.println("This is from First Anonymous Class ");
				
			}
		};
		
		inter.sayHello();
		
		
		MainInter inter2 = new MainInter() {
			
			@Override
			public void sayHello() {
				// TODO Auto-generated method stub
			System.out.println("This is From Second Anonymous Class");	
			}
		};
			
		inter2.sayHello();
	
	
//		The only Disadvantage is We need to Write This much Line of Code
		
		
		
//		Now we can write the same interface implementing Using Lambda (->)
		
		MainInter inter3 = ()->{
			System.out.println("this is From Lambda");};
		inter3.sayHello();
		
		
//		Now we can also {} if code is Single line as this
		
		MainInter i=()-> System.out.println("this is from 2nd Lambda");
		i.sayHello();
		
		
//		only disadvantage is only can be used by implementing interface which has one abstract method 
//		if interface has 2 abstract methods we can not use lambda at that situation we need to use method 2;
		
	}
	
	
}
